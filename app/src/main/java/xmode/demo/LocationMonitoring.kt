package xmode.demo

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.util.Log
import android.widget.Toast
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import com.google.android.gms.location.LocationServices
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class LocationMonitoringWorker: Worker() {
    override fun doWork(): Result {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)
        val latch = CountDownLatch(1)
        if (checkSelfPermission(applicationContext, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(applicationContext, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return Result.FAILURE
        }
        fusedLocationClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                Toast.makeText(applicationContext, it.toString(), Toast.LENGTH_LONG).show()
                Log.d(LocationMonitoringWorker::class.java.simpleName, it.toString())
                // TODO : save location to storage
                // 1) Save a simple text file with location & timestamp
                // 2) Create an SQLite DB and add a new row with location & timestamp
            }
        }
        latch.await(5, TimeUnit.SECONDS)
        startLocationMonitoring()
        return Result.SUCCESS
    }
}

/**
 * Start WorkerManager worker monitoring the user's location periodically
 */
fun startLocationMonitoring() {
    val delay: Long = if (BuildConfig.DEBUG) 1 else 60
    // WorkManager's shortest interval for recurring work is 1hr so it won't
    // work for debug frequency
    WorkManager.getInstance().enqueue(OneTimeWorkRequestBuilder<LocationMonitoringWorker>()
            .setInitialDelay(delay, TimeUnit.MINUTES)
            .build())
}
