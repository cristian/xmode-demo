package xmode.demo

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloWorld = findViewById<TextView>(R.id.id_text_view)
        val span = SpannableStringBuilder.valueOf(helloWorld.text).also {
            it.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View?) {
                    Toast.makeText(this@MainActivity, "Task #1 complete", Toast.LENGTH_LONG).show()
                    checkLocationPermission()
                }
            }, 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        helloWorld.text = span
        helloWorld.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startLocationMonitoring()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }

    /**
     * Check if location permission is granted. If so, start monitoring user's
     * location. If not, request the permission.
     */
    private fun checkLocationPermission() {
        if (checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this)
                        .setMessage("Cannot track location because permission not granted")
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSION_LOCATION)
            }
        } else {
            startLocationMonitoring()
        }
    }

    companion object {
        const val PERMISSION_LOCATION = 1
    }
}
